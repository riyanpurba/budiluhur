#include <iostream>
using namespace std;
int main()
{
	float nilai_a, nilai_b, nilai_c, rata_rata;

	cout << "Nilai A = ";
	cin >> nilai_a;
	cout << "Nilai B = ";
	cin >> nilai_b;
	cout << "Nilai C = ";
	cin >> nilai_c;
	cout << endl;
	rata_rata = (nilai_a + nilai_b + nilai_c) / 3;

	cout << "Rata-rata = " << rata_rata << endl;
	return 0;
}