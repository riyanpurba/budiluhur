# Perkuliahan

## Semester 1
### Logika Matematika
1. [Konsep Dasar Logika dan Negasi, Konjungsi, Disjungsi, Implikasi, dan Biimplikasi [Selasa, 23/08/2022]](https://gitlab.com/riyanpurba/budiluhur/-/tree/main/SEMESTER%231/LOGIKA%20MATEMATIKA/PERTEMUAN%231-28082022)
2. [Konvers, Invers, Kontraposisi, Tautologi, Kontradiksi, dan Kontigensi [Kamis, 25/08/2022]](https://gitlab.com/riyanpurba/budiluhur/-/tree/main/SEMESTER%231/LOGIKA%20MATEMATIKA/PERTEMUAN%232-25082022)
3. [Ekuivalensi dan Hukum Logika [Selasa, 30/08/2022]](https://gitlab.com/riyanpurba/budiluhur/-/tree/main/SEMESTER%231/LOGIKA%20MATEMATIKA/PERTEMUAN%233-30082022)
4. [Inferensi [Kamis, 01/09/2022]](https://gitlab.com/riyanpurba/budiluhur/-/tree/main/SEMESTER%231/LOGIKA%20MATEMATIKA/PERTEMUAN%234-01092022)
5. [Kuantor [Kamis, 06/09/2022]](https://gitlab.com/riyanpurba/budiluhur/-/tree/main/SEMESTER%231/LOGIKA%20MATEMATIKA/PERTEMUAN%235-06092022)
6. [Konsep Metode Pembuktian [Kamis, 08/09/2022]](https://gitlab.com/riyanpurba/budiluhur/-/tree/main/SEMESTER%231/LOGIKA%20MATEMATIKA/PERTEMUAN%236-08092022)

### Analis dan Desain Algoritma
1. [Pendahuluan [Rabu, 24/08/2022]](https://gitlab.com/riyanpurba/budiluhur/-/tree/main/SEMESTER%231/ANALIS%20DAN%20DESIGN%20ALGORITMA/PERTEMUAN%231-24082022)
2. [Tipe Data [Jum'at, 26/08/2022]](https://gitlab.com/riyanpurba/budiluhur/-/tree/main/SEMESTER%231/ANALIS%20DAN%20DESIGN%20ALGORITMA/PERTEMUAN%232-26082022)
3. [Kontrol If [Rabu, 31/08/2022]](https://gitlab.com/riyanpurba/budiluhur/-/tree/main/SEMESTER%231/ANALIS%20DAN%20DESIGN%20ALGORITMA/PERTEMUAN%233-31082022)
4. [Flowchart dan Kontrol If [Jum'at, 02/09/2022]](https://gitlab.com/riyanpurba/budiluhur/-/tree/main/SEMESTER%231/ANALIS%20DAN%20DESIGN%20ALGORITMA/PERTEMUAN%234-02092022)
5. [Kontrol Loop [Rabu, 07/09/2022]](https://gitlab.com/riyanpurba/budiluhur/-/tree/main/SEMESTER%231/ANALIS%20DAN%20DESIGN%20ALGORITMA/PERTEMUAN%235-07092022)
6. [Kontrol Nested Loop [Jum'at, 09/09/2022]](https://gitlab.com/riyanpurba/budiluhur/-/tree/main/SEMESTER%231/ANALIS%20DAN%20DESIGN%20ALGORITMA/PERTEMUAN%236-09092022)